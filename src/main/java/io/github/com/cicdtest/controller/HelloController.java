package io.github.com.cicdtest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author winfun
 * @date 2022/11/3 10:41 AM
 **/
@RestController
@RequestMapping("/hello")
public class HelloController {

    @GetMapping("{name}")
    public String sayHi(@PathVariable("name") String name) {
        return "Hi "+name;
    }
}
